
@extends('layouts.app')
@section('content')
    <br>
    <div class="form-group">
        <a class="btn btn-outline-primary" href="/todo/create" role="button">ประกาศงาน</a><br><br>
        <h1>ค้นหางาน</h1>
        <form class="form-inline my-2 my-lg-0"  method="get" action="{{url("/search")}}">
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="Search" aria-label="Search" style="width:1150px;">
        <br><br><br> <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

        <h2>งานที่ยังเปิดรับช่างภาพ</h2>
        @if(count($posts)>0)
            @foreach($posts as $post)
                <div>
                    <a href="{{ url('/todo/'.$post->id) }}">
                        <h3>{{$post->title}}</h3>
                    </a>
                    <p>{{$post->content}}</p>
                    <p>{{$post->due}}</p>
                    <img src="{{ url('uploads/'.$post->file_name) }}" width="129">
                </div>
                <hr>

    @endforeach
    @endif
        <a href="{{ url('/') }}" class="btn btn-secondary">Back</a>
@endsection


