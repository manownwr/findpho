
@extends('layouts.app')
@section('content')
    <br>
    <div class="form-group">
        <a class="btn btn-outline-primary" href="/todo/create" role="button">ประกาศงาน</a><br><br>
        <h1>ค้นหางาน</h1>
        <form class="form-inline my-2 my-lg-0"  method="get" action="{{url("/search")}}">
            <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="Search" aria-label="Search" style="width:1150px;">
            <br><br><br>  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    <br>

    <h2>งานที่ยังเปิดรับช่างภาพ</h2>
    @if(count($todos)>0)
        @foreach($todos as $todo)
            <div>
                <a href="{{ url('/todo/'.$todo->id) }}">
                <h3>{{$todo->title}}</h3>
                </a>
                <p>{{$todo->content}}</p>
                <p>{{$todo->due}}</p>
                <img src="{{ url('uploads/'.$todo->file_name) }}" width="129">
            </div>
            <hr>
            @endforeach
    @endif
@endsection


