<br>
@extends('layouts.app')
@section('content')
    <h1>ช่างภาพฝีมือดีกำลังรอคุณอยู่</h1>
    <form method="post" action="{{ url('/todo') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>ประเภทภาพถ่าย</label>
            <input type="text" name="title" class="form-control" value="ภาพบุคคล/ถ่ายแฟชั่น" >
        </div>
        <div class="form-group">
            <label>งบประมาณ</label>
            <input type="text" name="content" class="form-control" value="1,000-5,000">
        </div>
        <div class="form-group">
            <label>วันที่</label>
            <input type="date" name="due" class="form-control" value="{{ old('due') }}">
        </div>
        <div class="form-group">
            <label>ตัวอย่างภาพถ่าย</label>
            <input type="file" name="file" class="form-control" >
        </div>
        <button  type="submit">Confirm </button>
    </form>
@endsection
